import { Component } from '@angular/core';
import { AnalyticsService } from '../analytics.service';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  constructor(private analytics: AnalyticsService) {
  }

  ionViewDidEnter () {
    this.analytics.page({
      name: 'tab1'
    })
  }
}
