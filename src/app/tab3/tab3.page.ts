import { Component } from '@angular/core';
import { AnalyticsService } from '../analytics.service';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {
  constructor(private analytics: AnalyticsService) {
  }

  ionViewDidEnter () {
    this.analytics.page({
      name: 'tab3'
    })
  }
}
