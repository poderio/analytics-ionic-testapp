import { Component } from '@angular/core';
import { AnalyticsService } from '../analytics.service';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {
  constructor(private analytics: AnalyticsService) {
  }

  ionViewDidEnter () {
    this.analytics.page({
      name: 'tab2'
    })
  }

  sendTrack () {
    this.analytics.track({
      event: 'Button click'
    })
  }
}
