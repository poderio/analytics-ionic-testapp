import { Injectable } from '@angular/core';
import Analytics from 'analytics-node/dist/browserified.js';
import { Device } from '@ionic-native/device/ngx';

const analytics = new Analytics('TEST_WRITE_KEY');

@Injectable({
  providedIn: 'root'
})
export class AnalyticsService {
  analytics: any = analytics;
  advertisingId: String = '';
  adTrackingEnabled: Boolean = false;

  constructor(private device: Device) {
    document.addEventListener("deviceready", () => {
      // on iOS 'idfa' plugin is available only after 'deviceready' event
      (<any>cordova.plugins).idfa.getInfo().then((info) => {
        if (!info.limitAdTracking) {
          this.adTrackingEnabled = !info.limitAdTracking
          this.advertisingId = info.idfa || info.aaid
        }
      })
    }, false);
  }

  page (options) {
    this.analytics.page(this.extendOptions(options))
  }

  track (options) {
    this.analytics.track(this.extendOptions(options))
  }

  identify (options) {
    this.analytics.identify(this.extendOptions(options))
  }

  private extendOptions (options) {
    return {
      anonymousId: this.device.uuid,
      context: {
        app: {
          name: 'EpicaIonicExampleApp',
          version: '1',
          build: '0.0.1',
          namespace: 'ai.epica.analytics.ionic.testapp'
        },
        device: {
          advertisingId: this.advertisingId || undefined,
          adTrackingEnabled: this.adTrackingEnabled,
          cordova: this.device.cordova,
          model: this.device.model,
          platform: this.device.platform,
          uuid: this.device.uuid,
          version: this.device.version,
          manufacturer: this.device.manufacturer,
          isVirtual: this.device.isVirtual,
          serial: this.device.serial
        }
      },
      ...options
    }
  }
}
