# analytics-ionic-testapp

EPICA test app for IONIC iOS and Android analytics

# Development

To run an example app on iOS execute following command:

```
npm run dev:ios
```

To run an example app on Android execute following command:

```
npm run dev:android
```
